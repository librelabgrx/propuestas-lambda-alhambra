---
layout: post
category: talks
author: Ju Liu
title: Serving Millions of Users with Elm
---

## {{ page.author }} - {{ page.title }}

At NoRedInk we have one of the largest Elm codebases in the world. It is used by millions of teachers and students every day. In this talk, I will go through the structure of our codebase, then delineate some tecniques we keep the code clean and easy to mantain. By the end of the talk, you should be convinced that Elm and statically typed functional programming are "production ready".

## Proposal format

Indicate one of these:
-   [ ]  Short talk (10 minutes)
-   [x]  Talk (25 minutes)

## Description

I've planned this timeline for the talk:

- Introduce myself and NoRedInk (5 mins)
- Why Elm and why statically typed functional programming (5 mins)
- Structure of our codebase and how we write Elm (10 mins)
- Summary and conclusion (5 mins)


-   Project website:

## Target audiences

Lambda Aλhambra Day

## Speaker/s

Ju Liu

I've been working at NoRedInk for 4 years, building the product that students and teachers use every day. I have been writing Elm every day and I greatly enjoy the programming experience that comes with it.

### Contact/s

-   Name: Ju Liu
-   Email:
-   Personal website: <https://juliu.is>
-   Mastodon (or other free social networks):
-   Twitter: <https://twitter.com/arkh4m>
-   Gitlab:
-   Portfolio or GitHub (or other collaborative code sites): <https://github.com/Arkham>
