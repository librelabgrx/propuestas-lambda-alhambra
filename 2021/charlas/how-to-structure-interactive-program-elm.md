---
layout: post
category: talks
author: Daniel Trujillo Viedma
title: How to structure an interactive program - How I reinvented the Elm architecture.
---

## {{ page.author }} - {{ page.title }}

You've finished your introductory course on functional programming, and you know the basics of programs in this paradigm: data types, referential transparency, pure functions... You already know how to do things: Parse a document and print some information, command line calculator... You have your perfectly designed pure, functional core but, what about the impure shell around? Let's learn how to structure a relatively complex desktop application.

## Proposal format

Indicate one of these:
-   [x]  Short talk (10 minutes)
-   [ ]  Talk (25 minutes)

## Description

I recently made a somewhat complex desktop application trying to employ as much as possible the functional paradigm: a simulator of an old, popular, spanish 8-bits computer used by many universities to teach basic assembly language. As the hardware gets old, components get broken, rendering in many cases the computer unusable. Additionally, recent public health emergencies have greatly limited students' access to lab equipment, making the development of these activities hard. Since there aren't many options to easily develop and run programs in an simple assembly language, I decided to program a simulator of that computer with the additional purpose of enhacing my functional programming skills.

After designing and coding most of the purely functional core of the simulator: a function that takes the whole state of the simulation, and computes the next one, I got stuck with the designing of the impure shell that every purely functional program must have: the layer that talks to the outside. The big thinking time started.

A lot of hours washing dishes made me able to come up with ideas that lead to the final solution: Streams. If we could know beforehand the interactions between the outside world and our application, and we had a list of objects describing those interactions, then the problem is solved: Just fold the list with the initial simulation state, and write a function that takes the simulation, the interaction, and produces the result of the interaction in the simulation. And is at this point when you realise why there are so many stream processing libraries: Because an stream let's you abstract away the future interactions, so you can actually describe all your program in terms of fold.

Meanwhile you not only wash dishes, you also take the dog out for a walk, and you find in HackerNews an article about a cool language that abract away all the complete mess that is the web as of today. And it seems cool: it has Haskell-like syntax, a nice compiler (which is itself a major breaktrough compared to the fail-until-you-don't approach of Javascript), it's strongly typed, and purely functional. It seems so cool that even decide to read past the article's title, and discover how it internally works: you define a Model, which contains all the state of your program, and a function that returns an initial state. Then, you define a Message type, and a function that updates a Model, given a Message. And last, you define a view function that takes a Model a produces HTML.

Doesn't this sound familiar? It should, because it's nothing more than the solution we previously figured out for our impure layer (only that it's slightly broken apart).

A couple of months later this experience, I find this architecture in a lot of libraries, articles, and, in general, outside the Elm ecosystem. Seems we have found a simple, flexible and standard way of making this impure layer, that will take most of the pain away.

-   Project website: <https://git.sr.ht/~gdanix/Nansei>

## Target audiences

Learners, begginer users of the functional program paradigm, but also intermediate users that have proficiency with concepts like Monads, Functors and so on, but haven't made yet an interactive program in a functional way.

## Speaker/s

Daniel Trujillo Viedma is an spanish computer engineer very passionate about formal languages, and more precisely, strictly typed, functional-oriented programming languages. He has been a computer architecture professor at the University of Jaén, and is currently working in the Earth Sciences group at the BSC-CNS, making tools to help scientist assess the quality of climate-related datasets.

### Contact/s

-   Name: Daniel Trujillo Viedma
-   Email: <dtviedma@gmail.com>
-   Personal website:
-   Mastodon (or other free social networks):
-   Twitter:
-   GitLab:
-   Portfolio or GitHub (or other collaborative code sites): <https://git.sr.ht/~gdanix>
