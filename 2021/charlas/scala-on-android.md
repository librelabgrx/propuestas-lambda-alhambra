---
layout: post
category: talks
author: Maciej Gorywoda
title: Scala on Android
---

## {{ page.author }} - {{ page.title }}

Is there Scala on Android? Was there? Will there be?

For years we basically ignored the biggest market of JVM software on Earth. Those who decided to try anyway, were doomed to hack and patch through obsolete code just to stay afloat.

But is this the end? Or is there a way for Scala to come back?

## Proposal format

Indicate one of these:
-   [ ]  Short talk (10 minutes)
-   [x]  Talk (25 minutes)

## Description

For four years now I work professionally on the Android client of an end-to-end encrypted messenger, in Scala. In Android we deal a lot with events coming from many sources: the user, the backend, the Android OS itself… The code we write has to be very reactive - and it should also be concise and able to process all those events concurrently to squeeze all we can from limited resources. Scala should thrive under those conditions. And yet, it’s almost non-existent. People who still write it are forced to use old versions of libraries, on top of an old version of the language itself, to modify Gradle scripts, and basically to jump through countless loopholes which shouldn’t exist. Most of those people already either moved to other market niches… or to other programming languages.

In this talk, I want to outline how we ended up in this weird position. What attempts were made in the past to introduce Scala on Android, and how they failed. What hacks and concessions are needed in the present to still be able to write Scala on Android. But also I want to tell you about recent developments that give me a reason to believe that the future might be better.

-   Project website: <https://github.com/makingthematrix/scalaonandroid>

## Target audiences

Everyone interested in writing apps on Android.

## Speaker/s

Maciej Gorywoda.

Currently I'm employed at Wire in Berlin, working in Scala on the Android client of our end-to-end encrypted messenger. I write some small AI projects on the side, and learn Rust.

### Contact/s

-   Name: Maciej Gorywoda
-   Email: <gorywodamaciej@protonmail.com>
-   Personal website:
-   Mastodon (or other free social networks):
-   Twitter: <https://twitter.com/makingthematrix>
-   GitLab:
-   Portfolio or GitHub (or other collaborative code sites): <https://github.com/makingthematrix/scalaonandroid>

## Comments

In contrast to my other talks, I want this one to be less about the technical side. It should more on the inspirational side of things. I want to show people what is possible and make them curious.
