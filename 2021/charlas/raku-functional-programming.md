---
layout: post
category: talks
author: Juan Julián Merelo
title: Does it come in functional? Yes it does! Raku and functional programming
---

## {{ page.author }} - {{ page.title }}

Raku is "the language for the next 100 years", and it's quite obvious for everyone that every language will be functional from now on, so Raku can't escape that trend.
However, is it functional-like-Javascript or functional-like-Haskell? Well, something in between, but we will show how functional concepts are implemented in Raku, how natural and idiomatic they are, and how to leverage them in different design patterns in the language.

## Proposal format

Indicate one of these:
-   [ ]  Short talk (10 minutes)
-   [x]  Talk (25 minutes)

## Description

Raku was initially designed from the community of the Perl language, but it took off to be something new, modern, and different, gathering inspiration from many mainstream and edge concepts. Many functional features, including functions as first-class-citizens, are there, but there are many others related to features like composability or the type system, that can be leveraged to create pure functional modules and languages.

In this talk, we will go through different features of other functional languages like Haskell or Scala, and see how they are implemented in the Raku core, visiting different use cases and the curious syntax.

-   Project website: <https://raku.org>

## Target audiences

Anyone

## Speaker/s

JJ Merelo belongs to the Raku Steering Council and has been developing in Raku for the last 4 years.

### Contact/s

-   Name: Juan Julián Merelo
-   Email: <jjmerelo@gmail.com>
-   Personal website: <https://jj.github.io>
-   Mastodon (or other free social networks):
-   Twitter: <https://twitter.com/jjmerelo>
-   GitLab: <https://gitlab.com/JJ5>
-   Portfolio or GitHub (or other collaborative code sites): <https://github.com/JJ>

## Comments
