---
layout: post
category: talks
author: Filis Futsarov y Dawid Furman
title: PHP desde lo funcional over lemons
---

## {{ page.author }} - {{ page.title }}

El paradigma de programación funcional cada año se hace más conocido y ya se aplica en diferentes lenguajes y así ganando su buena fama. Todo esto gracias a comunidades y desarrolladores que están poniendo todo su esfuerzo en aplicarlo.

Tipos con sus álgebras nos aseguran que tanto en tiempo de compilación como en tiempo de ejecución no aparecen los errores como por ejemplo NullPointerException.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

La sintaxis de PHP permite expresar nuestro código de forma funcional. Durante la charla vamos a ver ejemplos básicos de:

* Funciones de primer orden.
* Funciones de orden superior (filter, map, reduce)
* Composición del funciones (compose)
* Y mucho más que nos permitirá romper la oscuridad que viene del mundo mutable (una demostración del ABC metrics)

-   Web del proyecto:

## Público objetivo

A todo el público.

## Ponente(s)

- Filis Futsarov
- Dawid Furman

### Contacto(s)

-   Nombre: Filis Futsarov
-   Email: filisfutsarov@gmail.com
-   Web personal:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/filisdev>
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/filisko>

## Comentarios
