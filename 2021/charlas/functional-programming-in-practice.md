---
layout: post
category: talks
author: Jens Grassel
title: Functional Programming in Practice - A journey
---

## {{ page.author }} - {{ page.title }}

A journey through the abysses and over the heights of the life in software development. Encouraging attendees to look into the domain of functional programming to gain new insights based upon the authors experience from over two decades in the industry.

## Proposal format

Indicate one of these:
-   [ ]  Short talk (10 minutes)
-   [x]  Talk (25 minutes)

## Description

Many have a dream if they choose to follow the road of software development. Reality looks different and sometimes very bleak. However not all hope is lost and functional programming is a way to again ignite this spark of joy and creation.

Furthermore we'll see that there are lots of things to discover and that exploration and discovery of re-useable abstractions paves the way for easing tedious tasks. In concrete the underrated Monoids and Semigroups are our entry followed by more powerful things.

Our journey takes us further and gives outlooks to IO, Optics, Refined Types, property based testing and encourages a visit into the land of "pure" functional programming.

-   Project website:

## Target audiences

Lambda Aλhambra Day

## Speaker/s

Jens Grassel

### Contact/s

-   Name: Jens Grassel
-   Email:
-   Personal website: <https://www.jan0sch.de>
-   Mastodon (or other free social networks): <https://chaos.social/@dwardoric>
-   Twitter: <https://twitter.com/dwardoric>
-   GitLab:
-   Portfolio or GitHub (or other collaborative code sites): <https://codeberg.org/jan0sch>

## Comments
